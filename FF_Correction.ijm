//@ File (label = "Image File", description="Image file, can be single image or stack") imFile
//@ File (label = "FF Image", description="Image file, can be single image or stack") ffFile
//@ String (label = "Transmitted Channel",description="If you have more than one channel, which would you like to process? Ignored for single channel data", value = "4") tChan

//-- Open both files and record names
open(ffFile);
ffTitle=getTitle;
//-- catch if FF image has more than one channel
if (nSlices()>1){Stack.setDisplayMode("grayscale");Stack.setChannel(tChan);}

open(imFile);
imgTitle=getTitle;
imgDir=File.directory;
imgName=File.nameWithoutExtension;
imgBD=bitDepth();
if (nSlices()>1){Stack.setDisplayMode("grayscale");Stack.setChannel(tChan);}

//-- create the FF corrected transmitted channel
imageCalculator("Divide create 32-bit", imgTitle,ffTitle);
close(ffTitle);
selectWindow("Result of "+imgTitle);

//-- match original bit depth
run(imgBD+"-bit");

//-- Copy the adjusted image back into the original stack
run("Copy");
selectWindow(imgTitle);
run("Paste");
run("Select None");

//-- Reset brightness and contrast and save
resetMinAndMax();
close("Result of "+imgTitle);
saveAs("Tiff", imgDir+File.separator+imgName+"_ff.tif");
close("*");