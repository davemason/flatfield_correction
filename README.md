# Flat field correction #
## FF_Correction.ijm ##
An ImageJ/ Fiji script to perform a basic flatfield correction. The theory is covered in [this post](https://postacquisition.wordpress.com/2018/05/23/correcting-the-record/), but briefly, the user is asked for both an image and a 'Flat Field' image (which is the same settings but with the sample removed). These are divided to produce a ratiometric image which is then re-sampled and re-saved to match the original file.

![plot](https://bitbucket.org/davemason/flatfield_correction/raw/master/example.png)

You can run it on multichannel images, just specify the correct transmitted channel in the initial dialog. If either the image or the FF image have a single channel, the choice of channel is ignored.

## Additional Files ##
The sample data from the post are also included as 16bit tiff files, as well as a script (make_artifact.ijm) used to create a semi-realistic optical artifact in an existing image.
	
## Acknowledgment ##

Written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk).