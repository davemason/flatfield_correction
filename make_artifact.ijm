//@ File (label = "Image File", description="Image file, can be single image or stack.") imFile

//-- Script to add smudges to lovely imaging data to demonstrate flat field correction

//-- Request an image, pull dimensions and bit depth
open(imFile);
title=getTitle();
getDimensions(w,h,c,s,f);
bd=bitDepth();

//-- Array of base coordinates and size parameters for ovals
xpos=newArray("0.55","0.64","0.75","0.7");
ypos=newArray("0.15","0.28","0.2","0.12");
spotsize=80;
spotVar=10;

//-- Create some ovals and add a bit of randomness to size
newImage("spot", bd+"-bit black", w, h, 1);
for (i=0;i<xpos.length;i++){
makeOval(floor(xpos[i]*w), floor(ypos[i]*h), floor(spotsize+randn()*spotVar), floor(spotsize+randn()*spotVar));
setForegroundColor(150, 150, 150);
run("Fill", "slice");
}
run("Select None");

//-- Blur the ovals
run("Gaussian Blur...", "sigma=10");
run("Gaussian Blur...", "sigma=80");
run("Copy");

//-- Now subtract the blur from the image to get a dark smudge
selectWindow(title);
setPasteMode("Subtract");
run("Paste");
run("Select None");
//close("spot"); //-- keep it open in case you want to apply to other images

//-- Functions
function randn() {
    //-- Use the Box-Muller method to produce normally distributed random numbers
    U=random();
    V=random();
    return sqrt(-2*log(U))*cos(2*3.14*V);
}